#!/usr/bin/env python3

'''
Käsurealt kasutamiseks:
$ ./create_condaenv.sh
$ conda list -p /home/.../gitlab_docker_elg_estspacysuntax/condaenv
$ ./condaenv/bin/python3 ./elg_sdk_et_dep_ud_spacy.py --json '{"type":"text","content":"Koer leidis kondi. Mees peeti kinni."}'

Veebiserver käsurealt:
$ ./condaenv/bin/python3 ./elg_sdk_et_dep_ud_spacy.py
$ curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' localhost:5000/process

Veebeserver konteinerist:
$ docker build -t tilluteenused/elg_estspacysuntax:2022.08.08 .
$ docker run -p 8000:8000 tilluteenused/elg_estspacysuntax:2022.08.08
$ curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' localhost:8000/process
'''

import json
import sys
import os
os.environ["PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION"] = "python"

from typing import Dict, List

from elg import FlaskService
from elg.model import TextsResponse, TextRequest, TextsResponseObject

class PIPELINE:
    """Load and run pipeline
    """
    def __init__(self, pl:str) -> None:
        """Load pipeline

        Args:
            pl (str): pipelane name {'et_dep_ud_sm'|'et_dep_ud_estbert'|'et_dep_ud_xlmroberta'}
        """
        self.pipeline_name = pl
        self.pipeline = None

    def doit(self, text:str) -> List:
        """Run pipeline

        Args:
            text (str): input text

        Returns:
            List: output from pipeline
        """
        if self.pipeline == None:
            #print("== Initsialiseerime:", self.pipeline_name)
            try:
                self.pipeline = __import__(self.pipeline_name).load()
            except ImportError:
                # Display error message
                print("Cant import pipeline:", self.pipeline_name, file=sys.stderr)
                return None

        doc = self.pipeline(text)

        texts_out = [] # array of sentences
        for sentence in doc.sents:
            sent_words = [] # array of tokens in current sentence
            for word in sentence:
                # current token
                txt = TextsResponseObject(content=str(word), features = {
                    "i": word.i,
                    "pos": word.pos_,
                    "morph": str(word.morph), 
                    "head": str(word.head.i) if word.head.i != word.i else None,
                    "dep": word.dep_,
                })
                sent_words.append(txt) # add current token to current sentence
            # add current sentence to list of sentences
            texts_out.append(TextsResponseObject(texts=sent_words))
        return texts_out # return list of sentences in text

#et_dep_ud_sm = PIPELINE('et_dep_ud_sm')        
#et_dep_ud_estbert = PIPELINE('et_dep_ud_estbert')
et_dep_ud_xlmroberta = PIPELINE('et_dep_ud_xlmroberta')

       
class ESTSPACY(FlaskService):
    def process_text(self, request) -> TextsResponse:
        #if request.params["pipeline"] == 'sm':
        #    texts_out = et_dep_ud_sm.doit(request.content)
        #elif request.params["pipeline"] == 'estbert':
        #    texts_out = et_dep_ud_estbert.doit(request.content)
        #elif request.params["pipeline"] == 'xlmroberta':
        #    texts_out = et_dep_ud_xlmroberta.doit(request.content)
        #else:
        #    texts_out = [] # wrong workflow name in params
        texts_out = et_dep_ud_xlmroberta.doit(request.content)
        return TextsResponse(texts=texts_out)
        
flask_service = ESTSPACY("EstSPACY")
app = flask_service.app

def run_test(my_query_str: str) -> None:
    my_query = json.loads(my_query_str)
    service = ESTSPACY("EstSPACY")
    #request = TextRequest(content=my_query["content"], params=my_query["params"])
    request = TextRequest(content=my_query["content"])
    response = service.process_text(request)

    response_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json = json.loads(response_str)
    return response_json

def run_server() -> None:
    app.run()

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='sentences to precrocess')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        json.dump(run_test(args.json), sys.stdout, indent=4)




