#!/bin/bash

# condaenv/bin/python3 elg_sdk_disamb.py --json '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}'
#
# conda activate /home/tarmo/gitlab-docker-elg/gitlab-docker-elg-disamb/condaenv
# ./elg_sdk_disamb.py --json '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}'
# conda deactivate

conda update -n base -c defaults conda
conda env create -f requirements_conda.yml --prefix ./condaenv --force

conda list -p ${HOME}/gitlab-docker-elg/gitlab_docker_elg_estspacysuntax/condaenv > conda-list.txt
